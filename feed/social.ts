import { DOMParser } from 'dom/deno-dom-wasm.ts';
import { ImageMimeType, Post } from '../queue/mod.ts';

/**
 * Get the social media (open) graph from the article's web page to populate the `Post`.
 * @param url The URL of the article page.
 * @returns A `Post` constructed from the open graph.
 */
async function getSocialGraph(url: string) {
	const request = await fetch(url);
	const body = await request.text();
	const document = new DOMParser().parseFromString(body, 'text/html');

	if (document) {
		const titleProperty = document.querySelector(
			'meta[property="og:title"]',
		);
		const descriptionProperty = document.querySelector(
			'meta[property="og:description"]',
		);
		const imageProperty = document.querySelector(
			'meta[property="og:image"]',
		);

		const titleContent = titleProperty?.getAttribute('content');
		const descriptionContent = descriptionProperty?.getAttribute('content');
		let imageContent = imageProperty?.getAttribute('content');

		if (!imageContent?.includes('://')) {
			imageContent = `${url}${imageContent}`;
		}

		return {
			uri: url,
			title: titleContent,
			description: descriptionContent,
			image: { uri: imageContent },
		} as Post;
	}

	return undefined;
}

/**
 * Get the image content from the article page's graph.
 * @param url URL for the image.
 * @returns Mime type and image buffer.
 */
async function getSocialImageContent(url: string) {
	const request = await fetch(url);
	const contentType = await request.headers.get(
		'Content-Type',
	) as ImageMimeType;

	const blob = await request.blob();
	const body = await blob.arrayBuffer();
	const uint = new Uint8Array(body);

	return {
		mimeType: contentType ?? 'image/jpeg',
		body: uint,
	};
}

export { getSocialGraph, getSocialImageContent, type Post };
