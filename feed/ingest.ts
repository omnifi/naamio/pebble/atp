/// <reference lib='deno.unstable' />

import { parseFeed } from 'rss';
import { FeedEntry } from 'rss/src/types/mod.ts';
import { getSocialGraph } from './social.ts';
import { Post } from '../queue/mod.ts';

/**
 * Expiry time (milliseconds) for the feed to stay alive in the data store. This is used
 * to automatically remove items from the feed that are too stale to post.
 *
 * This expiry time is also used as an indicator of which articles to ignore when parsing the
 * feed.
 */
const expiry = Number.parseInt(
	Deno.env.get('PEBBLE_FEED_EXPIRY') ?? '86400000',
);

/**
 * KV store prefix for the feed items.
 */
const feedKeyPrefix = ['pebble', 'news', 'feed'];

/**
 * Ingest the feed from a given URL into the data store and queue it for posting.
 *
 * @param url Path of the feed to ingest (e.g. RSS).
 */
async function ingestFeed(url: string) {
	const response = await fetch(url);
	const xml = await response.text();
	const { entries } = await parseFeed(xml);

	const today = new Date().getTime();
	const recentEntries = await entries.filter(
		(entry) => ((today - ((entry.published as Date).getTime())) < expiry),
	);

	console.debug(`${recentEntries.length} recent articles found in feed`);

	const kv = await Deno.openKv();

	await recentEntries.forEach(async (entry) => {
		const entryKey = new Array(...feedKeyPrefix);
		entryKey.push(entry.id);
		const savedEntry = await kv.get(entryKey);
		const feedEntry = savedEntry.value as FeedEntry;

		if (!feedEntry) {
			await kv.set(entryKey, entry, { expireIn: expiry });

			const postURI = entry.links[0].href;

			if (postURI) {
				const post = await createPost(postURI);

				if (post) {
					await queuePost(post);
				}
			}
		}
	});
}

/**
 * Create a `Post` from an item in the feed.
 *
 * @param uri URI for the article.
 * @returns A `Post` generated from OpenGraph data in the article page.
 */
async function createPost(uri: string) {
	const post = await getSocialGraph(uri);

	return post;
}

/**
 * Queue a `Post` for submission to the social network.
 * @param post `Post` to queue.
 */
async function queuePost(post: Post) {
	const kv = await Deno.openKv();

	const delay = 10000; // Nominal 10-second day. No delay is necessary currently.
	// Enqueue the message for immediate delivery
	await kv.enqueue(post, { delay });
}

export { ingestFeed };
