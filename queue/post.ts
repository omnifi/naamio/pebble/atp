type ImageMimeType = 'image/jpeg' | 'image/png' | 'image/webp';

/**
 * Post is used to provide the simplest structure for a social feed post, aligning
 * with syndicated content expectations.
 */
interface Post {
	/**
	 * URI for the post.
	 */
	uri: string;
	/**
	 * Title of the post.
	 */
	title: string;
	/**
	 * Description / summary / synopsis of the post.
	 */
	description: string;
	/**
	 * Main feature image representing the post.
	 */
	image: {
		/**
		 * URI for the image.
		 */
		uri?: string;
		/**
		 * Internal conent of the image.
		 */
		content?: Uint8Array;
		/**
		 * Image mime type.
		 */
		mimeType: ImageMimeType;
	};
}

// Type guard to check whether the message in the queue is a `Post`.
function isPost(post: unknown): post is Post {
	return (
		((post as Post)?.uri !== undefined) &&
		((post as Post)?.title !== undefined) &&
		((post as Post)?.description !== undefined)
	);
}

export default Post;

export { type ImageMimeType, isPost, type Post };
