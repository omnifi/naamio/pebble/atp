import atproto from 'npm:@atproto/api';

import { isPost } from '../queue/mod.ts';
import {
	getSocialGraph,
	getSocialImageContent,
	type Post,
} from '../feed/mod.ts';
import { modifyImage } from '../media/mod.ts';

/**
 * Expiry time (milliseconds) for when posts will expire from the data store.
 */
const expiry = Number.parseInt(
	Deno.env.get('PEBBLE_POST_EXPIRY') ?? '86400000',
);

/**
 * Maximum number of posts that can be submitted per run. It is recommended that
 * this is left at 1 as it can otherwise be unpleasant to see multiple posts
 * appear at the same time.
 */
const maximumPostSize = Number.parseInt(
	Deno.env.get('PEBBLE_MAX_POST_SIZE') ?? '1',
);

/**
 * Key value store prefix for posts.
 */
const postKeyPrefix = ['pebble', 'news', 'posts'];

/**
 * Deno KV store.
 */
const kv = await Deno.openKv();

/**
 * Listen for new posts arriving in the queue.
 */
async function listenForPost() {
	await kv.listenQueue(postReceived);
}

/**
 * Event handler for posts being received in the queue.
 *
 * @param post Potential `Post`.
 */
async function postReceived(post: unknown) {
	if (isPost(post)) {
		const socialGraph = await getSocialGraph((post as Post).uri);

		if (socialGraph) {
			queuePost(socialGraph);
		}
	} else {
		console.error(`unknown message received: ${post}`);
	}
}

/**
 * Queue the post in the KV store for posting.
 *
 * @param post `Post` to submit to the social network.
 */
async function queuePost(post: Post) {
	const entryKey = new Array(...postKeyPrefix);
	entryKey.push(post.uri);

	await kv.set(entryKey, post, { expireIn: expiry });
}

/**
 * Post all available `Post` items in the KV store, not exceeding the
 * maximum post size.
 */
async function post() {
	const posts = kv.list({ prefix: postKeyPrefix });

	for (let index = 0; index < maximumPostSize; index++) {
		const entry = (await posts.next()).value;

		if (entry) {
			const key = entry.key;
			const post = entry.value as Post;

			if (await recentlyPosted(post)) {
				console.log(`too soon to post duplicate — '${post.uri}`);

				index--; // Reset the count as this post has already been submitted.
			} else {
				await postItem(post);
			}

			await kv.delete(key);
		} else {
			break;
		}
	}
}

/**
 * Post an individual `Post` to the social network.
 * @param post `Post` to submit to the social network.
 * @returns `void` used for an abrupt break.
 */
async function postItem(post: Post) {
	if (post.image.uri) {
		// Download the image from the original source.
		const socialImageContent = await getSocialImageContent(
			post?.image.uri,
		);

		// Modify the image to be no more than 512 pixels wide (aspect ratio will be maintained).
		const modifiedImage = await modifyImage(
			new Uint8Array(socialImageContent.body),
			{
				width: 512,
				height: 0,
				mode: 'resize',
			},
		);

		// Set the mimetype to the same as the original image.
		if (socialImageContent) {
			post.image.mimeType = socialImageContent.mimeType;
			post.image.content = modifiedImage;
		}

		// Get the ATP agent for posting.
		const agent = await getAgent();

		if (!agent) {
			console.error('unable to log in to the AT Protocol network');

			return;
		}

		// Upload the image blob as a thumbnail.
		const uploadResponse = await agent.uploadBlob(modifiedImage, {
			headers: {
				'Content-Type': post.image.mimeType,
			},
			encoding: post.image.mimeType,
		});

		// Create the rich text of the post for potential features.
		const rt = new atproto.RichText({
			text: `${post.description}`,
		});

		// Submit the `Post` to ATP.
		const postResponse = await agent.post({
			$type: 'app.bsky.feed.post',
			text: rt.text,
			facets: rt.facets,
			createdAt: new Date().toISOString(),
			embed: {
				$type: 'app.bsky.embed.external',
				external: {
					uri: post.uri,
					thumb: uploadResponse.data.blob,
					title: post.title,
					description: post.description,
				},
			},
			langs: [
				'en', // TODO: use the RSS feed language for posts.
			],
		});

		console.log(`posting '${post.title}' at ${postResponse.uri}`);
	}
}

/**
 * Check guard to ensure posts are not submitted more than once.
 *
 * Posts that have been submitted are typically removed from the KV store, but
 * in the event of an abrupt failure, the program may not be able to remove a
 * post before exiting.
 *
 * @param post `Post` to check on the social network.
 * @returns True / false flag indicating whether the post is already in the social network.
 * This can potentially return false negatives if the network is not reachable.
 */
async function recentlyPosted(post: Post) {
	const agent = await getAgent();
	const actor = Deno.env.get('PEBBLE_ATP_ACTOR');

	if (!agent) {
		console.error('unable to log in to the AT Protocol network');

		return false;
	}

	if (!actor) {
		console.error('actor not set. unable to get feed');

		return false;
	}

	const posts = await agent.getAuthorFeed({
		actor: actor,
	});

	const posted = posts.data.feed.some((item) => {
		const external = item.post.embed?.external as { uri: string };

		if (external) {
			if (external.uri === post.uri) {
				return true;
			}
		}
	});

	return posted;
}

/**
 * Get the ATP agent for connecting to the social network.
 * @returns An ATP agent.
 */
async function getAgent() {
	const agent = new atproto.BskyAgent({ service: 'https://bsky.social' });

	const identifier = Deno.env.get('PEBBLE_ATP_IDENTIFIER');
	const password = Deno.env.get('PEBBLE_ATP_PASSWORD');

	if (identifier && password) {
		await agent.login({
			identifier: identifier,
			password: password,
		});

		return agent;
	}
}

export { listenForPost, post };
