# Pebble ATP

A lightweight program running on Deno that takes (RSS) feeds and submits new posts to the AT protocol network in a timely fashion. 

It's intended as a demonstration of both Deno's edge network and platform API, and the AT protocol API.

## Use of Deno

Through [Deno's](https://deno.com) API this program makes use of:

 - [scheduled tasks](https://docs.deno.com/deploy/kv/manual/cron) in the form of [cron](https://en.wikipedia.org/wiki/Cron) schedules;
 - [key value (KV) store](https://docs.deno.com/deploy/kv/manual/) built on [FoundationDB](https://www.foundationdb.org); and
 - [queues](https://docs.deno.com/deploy/kv/manual/queue_overview) which utilize the KV stores.

### Deployment considerations

Deno Deploy is used as a fully managed edge service in our live use case. Within the limits of our project Deno Deploy is free and provides **deployments in 35 data-centers** at the time of writing, worldwide, with **sub-1-second start times**, and **sub-50-millisecond response times**. Deployment uses [deployctl](https://docs.deno.com/deploy/manual/deployctl) which is simple and quick to update the service.

## Environment variables

There are several environment variables that are needed for the program to run fully. Some may be missed and the program will still operate with only partial functionality. This has not been tested fully, so it is recommended to use all environment variables.

 - `PEBBLE_MAX_POST_SIZE`=(_maximum number of posts to be submitted in each run_)
 - `PEBBLE_FEED_EXPIRY`=(_expiry for items from feeds in milliseconds_)
 - `PEBBLE_POST_EXPIRY`=(_expiry for old posts in milliseconds_)
 - `PEBBLE_FEED_URLS`=(_JSON array of URLs to add to the feed_)
 - `PEBBLE_FEED_CRON`=(_cron schedule for aggregating new feeds_)
 - `PEBBLE_POST_CRON`=(_cron schedule for submitting new posts_)
 - `PEBBLE_ATP_IDENTIFIER`=(_identifier for the AT protocol account_)
 - `PEBBLE_ATP_PASSWORD`=(_application password from the AT protocol_)
 - `PEBBLE_ATP_ACTOR`=(_AT protocol account handle to use for checking the feed_)
 - `PEBBLE_DID`=(_DID from the AT protocol network, used to validate the account_)
