import { ServeInit } from '$std/http/server.ts';
import {
	json,
	Routes,
	serve,
	validateRequest,
} from 'sift';

/**
 * Starts the HTTP server for routing to relevant endpoints. This is a simple
 * implementation as there is no functional service as yet.
 *
 * Essentially we are returning two endpoints:
 *   - `/.well-known/atproto-did` for the ATP DID.
 *   - `/health` for a health check to keep the service alive.
 */
// deno-lint-ignore require-await
async function startServer() {
	const routes: Routes = {
		'/.well-known/atproto-did': handleDID,
		'/health': handleHealth,
	};

	const options: ServeInit = {
		port: 80,
	};

	serve(routes, options);
}

/**
 * Handles the ATP DID request for the ATP network. The DID value is set as
 * an environment variable: `PEBBLE_DID`.
 *
 * @param request HTTP request.
 * @returns HTTP response.
 */
async function handleDID(request: Request) {
	const did = Deno.env.get('PEBBLE_DID');

	if (!did) {
		return json({ error: 'did not set' }, { status: 500 });
	}

	// Make sure the request is a GET request.
	const { error } = await validateRequest(request, {
		GET: {},
	});

	// validateRequest populates the error if the request doesn't meet
	// the schema we defined.
	if (error) {
		return json({ error: error.message }, { status: error.status });
	}

	const headers = new Headers({
		'content-type': 'text/plain',
	});

	// Return all the quotes.
	return new Response(did, { headers });
}

/**
 * Handles the health request.
 *
 * @param request HTTP request.
 * @returns HTTP response.
 */
async function handleHealth(request: Request) {
	// Make sure the request is a GET request.
	const { error } = await validateRequest(request, {
		GET: {},
	});

	// validateRequest populates the error if the request doesn't meet
	// the schema we defined.
	if (error) {
		return json({ error: error.message }, { status: error.status });
	}

	const acceptHeader = request.headers.get('Accept');

	if (acceptHeader) {
		if (acceptHeader.includes('application/json')) {
			const headers = new Headers({
				'content-type': 'application/json',
			});

			return json({ status: 200 }, { headers: headers, status: 200 });
		}
	}

	const headers = new Headers({
		'content-type': 'text/plain',
	});

	return new Response('service is okay', { headers: headers, status: 200 });
}

export { startServer };
