import {
	ImageMagick,
	initializeImageMagick,
	MagickGeometry,
} from '@imagemagick';

/**
 * ImageMagick is a WebAssembly package. It requires initializing before using.
 */
await initializeImageMagick();

/**
 * Resize the image to ensure it's suitable for the social network.
 *
 * Note: The ATP network has strict limits on image sizes so this is needed to ensure
 * posts do not fail the check.
 *
 * @param imageBuffer Image buffer.
 * @param params Modification parameters (width, height, mode (resize or crop)).
 * @returns Updated image buffer with resized image.
 */
function modifyImage(
	imageBuffer: Uint8Array,
	params: { width: number; height: number; mode: string },
) {
	const sizingData = new MagickGeometry(
		params.width,
		params.height,
	);

	sizingData.ignoreAspectRatio = params.height > 0 && params.width > 0;

	return new Promise<Uint8Array>((resolve) => {
		ImageMagick.read(imageBuffer, (image) => {
			image.resize(sizingData);
			image.write((data) => resolve(data));
		});
	});
}

export { modifyImage };
