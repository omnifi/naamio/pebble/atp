import { listenForPost } from './post/post.ts';
import { ingestFeed } from './feed/mod.ts';
import { post } from './post/mod.ts';
import { startServer } from './server/server.ts';

/**
 * Feed URL environment variable.
 */
const feedURLsVariable = Deno.env.get('PEBBLE_FEED_URLS');

/**
 * Feed ingest cron schedule environment variable.
 */
const feedCronVariable = Deno.env.get('PEBBLE_FEED_CRON');

// Check if environment variables have been set. If not, then the feed is not ingested.
// The rest of the program operates as normal, meaning we do not need to exit.
if (feedURLsVariable && feedCronVariable) {
	const feedURLs = JSON.parse(feedURLsVariable) as [];

	Deno.cron('ingest', feedCronVariable, async () => {
		await feedURLs.forEach(async (url) => {
			await ingestFeed(url);
		});
	});

	// Activates the queue for posts. Posts can be queued even if the feed and
	// post tasks are not active. Once active, they can resume to pick up items
	// from the queue.
	listenForPost();
}

/**
 * Post cron job schedule environment variable.
 */
const postCronVariable = Deno.env.get('PEBBLE_POST_CRON');

// If the post environment variable is set then activate the cron task runner.
if (postCronVariable) {
	Deno.cron('post', postCronVariable, async () => {
		await post();
	});
}

await startServer();
